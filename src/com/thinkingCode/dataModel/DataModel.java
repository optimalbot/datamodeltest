package com.thinkingCode.dataModel;

import java.util.ArrayList;
import java.util.List;

public class DataModel {

	
	public List<List<KeyValueModel>> Data = new ArrayList<List<KeyValueModel>>();;
	
	public DataModel() {
		
		//Create a new list of key values (band)
		List<KeyValueModel> bandOne = new ArrayList<KeyValueModel>() ;
		//Add some data to it 
		KeyValueModel pairOne = new KeyValueModel("KeyOne", "ValueOne");
		KeyValueModel pairTwo = new KeyValueModel("KeyTwo", "ValueTwo");
		//Add data to band
		bandOne.add(pairOne);
		bandOne.add(pairTwo);
		//Add this to Data
		Data.add(bandOne);
		
		//Create a new list of key values (band)
		List<KeyValueModel> bandTwo = new ArrayList<KeyValueModel>() ;
		//Add some data to it 
		KeyValueModel pairThree = new KeyValueModel("KeyThree", "ValueThree");
		KeyValueModel pairFour = new KeyValueModel("KeyFour", "ValueFour");
		KeyValueModel pairFive = new KeyValueModel("KeyFive", "ValueFive");
		//Add data to band
		bandTwo.add(pairThree);
		bandTwo.add(pairFour);
		bandTwo.add(pairFive);
		//Add this to Data
		Data.add(bandTwo);
		
	}
	
}
