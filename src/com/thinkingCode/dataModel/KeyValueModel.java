package com.thinkingCode.dataModel;

import java.io.Serializable;

@SuppressWarnings("serial")
public class KeyValueModel implements Serializable {

	private String Key;
	private String Value;
	
	
	public KeyValueModel(String key, String value) {
		this.Key = key;
		this.Value = value;
	}
	public void setKey(String key) {
		this.Key = key;
		
	}
	public void setValue(String value) {
		this.Value = value;
		
	}
	public String getKey() {
		return this.Key;
	}
	public String getValue() {
		return this.Value;
	}
}

